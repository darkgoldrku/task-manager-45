package ru.t1.bugakov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.AbstractUserRequest;
import ru.t1.bugakov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class TaskChangeStatusByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    @Nullable
    private Status status;

    public TaskChangeStatusByIdRequest(@Nullable String id, @Nullable Status status) {
        this.id = id;
        this.status = status;
    }

    public TaskChangeStatusByIdRequest(@Nullable String token, @Nullable String id, @Nullable Status status) {
        super(token);
        this.id = id;
        this.status = status;
    }

}
