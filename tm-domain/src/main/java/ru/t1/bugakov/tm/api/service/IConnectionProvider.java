package ru.t1.bugakov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IConnectionProvider extends IDataBaseProperty {

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

    @NotNull String getSessionKey();

    int getSessionTimeout();
}
