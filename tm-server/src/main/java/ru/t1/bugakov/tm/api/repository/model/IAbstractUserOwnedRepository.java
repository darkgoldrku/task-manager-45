package ru.t1.bugakov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IAbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends IAbstractRepository<M> {

    void add(@NotNull String userId, M model) throws Exception;

    void update(@NotNull String userId, M model);

    void remove(@NotNull String userId, M model);

    void removeById(@NotNull String userId, @NotNull String id);

    boolean existsById(@NotNull String userId, @NotNull String id);

    void clear(@NotNull String userId);

    @Nullable List<M> findAll(@NotNull String userId);

    @Nullable List<M> findAllWithSort(@NotNull String userId, @Nullable String sortField);

    @Nullable M findById(@NotNull String userId, @NotNull String id);

    int getSize(@NotNull String userId);

}
