package ru.t1.bugakov.tm.api.service.model;

import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;

public interface IProjectTaskService {

    void bindTaskToProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) throws SQLException;

    void unbindTaskToProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) throws SQLException;

    void removeProjectById(@Nullable final String userId, @Nullable final String projectId) throws Exception;

}
