package ru.t1.bugakov.tm.service;

import lombok.SneakyThrows;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.service.IConnectionService;
import ru.t1.bugakov.tm.api.service.IDataBaseProperty;
import ru.t1.bugakov.tm.dto.model.ProjectDTO;
import ru.t1.bugakov.tm.dto.model.SessionDTO;
import ru.t1.bugakov.tm.dto.model.TaskDTO;
import ru.t1.bugakov.tm.dto.model.UserDTO;
import ru.t1.bugakov.tm.model.Project;
import ru.t1.bugakov.tm.model.Session;
import ru.t1.bugakov.tm.model.Task;
import ru.t1.bugakov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IDataBaseProperty dataBaseProperties;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull IDataBaseProperty dataBaseProperties) {
        this.dataBaseProperties = dataBaseProperties;
        this.entityManagerFactory = factory();
    }

    @NotNull
    @Override
    @SneakyThrows
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    public EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, dataBaseProperties.getDBDriver());
        settings.put(Environment.URL, dataBaseProperties.getDBUrl());
        settings.put(Environment.USER, dataBaseProperties.getDBUsername());
        settings.put(Environment.PASS, dataBaseProperties.getDBPassword());
        settings.put(Environment.DIALECT, dataBaseProperties.getDBDialect());
        settings.put(Environment.HBM2DDL_AUTO, dataBaseProperties.getDBHbm2ddlAuto());
        settings.put(Environment.SHOW_SQL, dataBaseProperties.getDBShowSql());
        settings.put(Environment.FORMAT_SQL, dataBaseProperties.getDBFormatSql());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, dataBaseProperties.getDBSecondLvlCache());
        settings.put(Environment.CACHE_REGION_FACTORY, dataBaseProperties.getDBFactoryClass());
        settings.put(Environment.USE_QUERY_CACHE, dataBaseProperties.getDBUseQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, dataBaseProperties.getDBUseMinPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, dataBaseProperties.getDBRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, dataBaseProperties.getDBConfigFilePath());

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(UserDTO.class);

        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(Session.class);

        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}
