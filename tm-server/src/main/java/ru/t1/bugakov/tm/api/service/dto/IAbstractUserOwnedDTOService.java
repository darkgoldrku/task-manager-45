package ru.t1.bugakov.tm.api.service.dto;

import ru.t1.bugakov.tm.api.repository.dto.IAbstractUserOwnedDTORepository;
import ru.t1.bugakov.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IAbstractUserOwnedDTOService<M extends AbstractUserOwnedModelDTO> extends IAbstractUserOwnedDTORepository<M> {

}
